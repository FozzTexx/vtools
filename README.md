## About

These tools are for working with Victor 9000 disk images to move files
around. Currently only MSDOS sector images are supported but I hope to
add CP/M support soon. Both single sided and double sided floppy
images are supported.

The main use is for creating new floppy disk images and then using
[floppur](https://gitlab.com/FozzTexx/floppur) to convert the sector
images to flux images and then writing them out for use on real Victor
9000 / Sirius 1 machines. **Yes, that's right,** with this tool and
floppur it's possible to create your own Victor 9000 floppies without
having any bootable media beforehand.

The vcopy tool is able to create a new blank image if the specified
sector image file doesn't already exist. It can create single sided
images, or if the --doublesided flag is used, it will create a double
sided image. It will also recognize when copying MSDOS.SYS onto a disk
and will reshuffle clusters as needed to make sure all sectors of
MSDOS.SYS are in sequential order and will update the floppy label at
sector 0 to point to the first sector of MSDOS.SYS.

Chris Osborn - [@FozzTexx](https://twitter.com/FozzTexx) - [insentricity.com](https://www.insentricity.com/a.cl/286/making-victor-9000-disks-without-a-victor-9000) - fozztexx@fozztexx.com

### Requirements

You will need my [floppur package](https://gitlab.com/FozzTexx/floppur) installed.

## Usage

To get a directory listing:

`vdir v9kdisk.img`

To copy a file onto a disk:

`vcopy v9kdisk.img somefile.exe :`

To copy a file off:

`vcopy v9kdisk.img :readme.txt .`

To copy a file onto a disk with a different destination name:

`vcopy v9kdisk.img file.txt :readme.txt`

## License

GPL 3
