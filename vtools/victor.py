##########################################################################
# Copyright 2022 by Chris Osborn <fozztexx@fozztexx.com>
#
# This file is part of vtools.
#
#  vtools is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  vtools is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with vtools.  If not, see <http://www.gnu.org/licenses/>.
##########################################################################

import os, sys
import struct
import datetime
from vtools.fat import DirectoryEntry, FAT
from fzfloppur.victorimage import VictorImage as VictorFloppy

class VictorHD:
  hardKeys = ["loadableID:H", "revision:H", "serial:16s", "sectorSize:H", "bootStart:I",
              "startSegment:H", "systemSize:H", "loadAddress:I", "primaryVolume:H",
              "driveParameters:8s", "driveOptions:B", "interleave:B", "badTrackCount:H",
              "spare:I", "partitionTable:972s"]
  
  partitionKeys = ["type:H", "partitionID:16s", "bootStart:I", "startSegment:H",
                   "systemSize:H", "loadAddress:I", "partitionSectors:I", "unknown1:4s",
                   "sectorSize:H", "clusterSize:H", "maxEntries:H", "unknown2:16s",
                   "assignCount:B", "driveAssignment:64s"]

  def __init__(self, path, partition=0):
    self.path = path
    self.partition = partition
    return

  # FIXME - might need some way to report number of fat sectors and root directory sectors
  # FIXME - might need some way to report cluster size
  
  def readSector(self, sector):
    # FIXME - read sector of current partition
    return

  def writeSector(self, sector):
    # FIXME - write sector to current partition
    return
  
class VictorImage(VictorFloppy):
  floppyKeys = ["loadableID", "loadAddress", "systemSize", "startOffset", "startSegment",
              "diskID", "systemID", "sectorSize", "dataStart", "bootStart", "flags",
              "interleave", "diskVersion", "reserved", "speedControl", "zoneTable",
              "sectorsPerTrack", "unknown", "checksum"]
  floppyStruct = "<HHHHH 8s 8s HHH BBB 3s 18s 15s 15s 42s H"
  hardKeys = ["loadableID", "revision", "serial", "sectorSize", "bootStart", "startSegment",
              "systemSize", "loadAddress", "primaryVolume", "driveParameters", "driveOptions",
              "interleave", "spare", "partitionTable"]
  hardStruct = "<HH 16s HIHHIH 8s BB 6s 972s"
  partitionKeys = ["loadableID", "partitionID", "bootStart", "startSegment", "loadAddress",
                   "unknown1", "partitionSize", "unknown2", "sectorSize", "clusterSize",
                   "maxEntries", "unknown2"]
  partitionStruct = "<H 16s IHIHI 4s HHH 84s"
  
  def __init__(self, path):
    super().__init__(path)

    self.label = self.unpackLabel()
    self.format = FAT(self)
    return

  def unpackLabel(self):
    data = self.readAbsoluteSector(0)
    slen = struct.calcsize(VictorImage.floppyStruct)
    values = struct.unpack(VictorImage.floppyStruct, data[:slen])
    label = dict(zip(VictorImage.floppyKeys, values))
    label['speedControl'] = list(label['speedControl'])
    label['zoneTable'] = list(label['zoneTable'])
    label['sectorsPerTrack'] = list(label['sectorsPerTrack'])
    return label

  def packLabel(self):
    label = self.label.copy()
    label['speedControl'] = bytes(label['speedControl'])
    label['zoneTable'] = bytes(label['zoneTable'])
    label['sectorsPerTrack'] = bytes(label['sectorsPerTrack'])
    values = []
    for key in VictorImage.floppyKeys:
      values.append(label[key])
    bin_label = struct.pack(VictorImage.floppyStruct, *values)
    label['checksum'] = sum(bin_label[:-2]) & 0xffff
    bin_label = bin_label[:-2] + label['checksum'].to_bytes(2, "little")
    data = self.readAbsoluteSector(0)
    data[0:len(bin_label)] = bin_label
    self.writeAbsoluteSector(0, data)
    return
  
  def absoluteSectorToOffset(self, sector):
    # FIXME - don't assume data is packed/triangular
    offset = sector * self.sector_size
    return offset

  def readAbsoluteSector(self, sector):
    offset = self.absoluteSectorToOffset(sector)
    return self.data[offset:offset+self.sector_size]

  def writeAbsoluteSector(self, sector, data):
    offset = self.absoluteSectorToOffset(sector)
    if len(data) != self.sector_size:
      raise ValueError("Invalid sector size", len(data), self.sector_size)
    self.data[offset:offset+self.sector_size] = data
    return

  # Methods forwarded to format handler
  def loadFile(self, *method_args, **method_kwargs):
    return self.format.loadFile(*method_args, **method_kwargs)

  def saveFile(self, *method_args, **method_kwargs):
    return self.format.saveFile(*method_args, **method_kwargs)

  def deleteFile(self, *method_args, **method_kwargs):
    return self.format.deleteFile(*method_args, **method_kwargs)

  def updateFATSectors(self, *method_args, **method_kwargs):
    return self.format.updateFATSectors(*method_args, **method_kwargs)

  def readCluster(self, *method_args, **method_kwargs):
    return self.format.readCluster(*method_args, **method_kwargs)

  def writeCluster(self, *method_args, **method_kwargs):
    return self.format.writeCluster(*method_args, **method_kwargs)

  def loadDirectory(self, *method_args, **method_kwargs):
    return self.format.loadDirectory(*method_args, **method_kwargs)
                
  def createDirectoryEntry(self, *method_args, **method_kwargs):
    return self.format.createDirectoryEntry(*method_args, **method_kwargs)

  def updateDirectory(self, *method_args, **method_kwargs):
    return self.format.updateDirectory(*method_args, **method_kwargs)

  def findEntryForCluster(self, *method_args, **method_kwargs):
    return self.format.findEntryForCluster(*method_args, **method_kwargs)

  def clusterChain(self, *method_args, **method_kwargs):
    return self.format.clusterChain(*method_args, **method_kwargs)

  def firstSectorOfCluster(self, *method_args, **method_kwargs):
    return self.format.firstSectorOfCluster(*method_args, **method_kwargs)

  def glob(self, *method_args, **method_kwargs):
    return self.format.glob(*method_args, **method_kwargs)
