##########################################################################
# Copyright 2022 by Chris Osborn <fozztexx@fozztexx.com>
#
# This file is part of vtools.
#
#  vtools is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  vtools is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with vtools.  If not, see <http://www.gnu.org/licenses/>.
##########################################################################

import datetime
import struct
import fnmatch
import sys
import re

DIR_STRUCT = "<8s3s B H HHH H HHH L"
SECTOR_FAT = 1
ENTRY_LEN = 32

class DirectoryEntry:
  ATTRIB_READONLY = 0x01
  ATTRIB_HIDDEN = 0x02
  ATTRIB_SYSTEM = 0x04
  ATTRIB_VOLUMELABEL = 0x08
  ATTRIB_SUBDIR = 0x10
  ATTRIB_ARCHIVE = 0x20

  def __init__(self, data):
    self.filename = None
    self.extension = None
    self.attributes = 0x20
    self.reserved = 0

    # d, t = self.encodeTimestamp(datetime.datetime.now())
    # self.creationTime = t
    # self.creationDate = d

    # Victor MS-DOS seems to hate creation date/time
    self.creationTime = 0
    self.creationDate = 0

    self.lastAccessDate = 0
    self.ignore = 0
    self.lastWriteTime = 0
    self.lastWriteDate = 0
    self.firstLogicalCluster = None
    self.fileSize = None

    if data is not None:
      entry = struct.unpack(DIR_STRUCT, data)
      self.filename = entry[0]
      self.extension = entry[1]
      self.attributes = entry[2]
      self.reserved = entry[3]
      self.creationTime = entry[4]
      self.creationDate = entry[5]
      self.lastAccessDate = entry[6]
      self.ignore = entry[7]
      self.lastWriteTime = entry[8]
      self.lastWriteDate = entry[9]
      self.firstLogicalCluster = entry[10]
      self.fileSize = entry[11]

    return

  def markAsDeleted(self):
    self.filename = b'\xE5' + self.filename[1:]
    return

  def pack(self):
    return struct.pack(DIR_STRUCT,
                       self.filename, self.extension, self.attributes, self.reserved,
                       self.creationTime, self.creationDate, self.lastAccessDate,
                       self.ignore, self.lastWriteTime, self.lastWriteDate,
                       self.firstLogicalCluster, self.fileSize)

  @staticmethod
  def decodeTimestamp(date, time=0):
    year = 1980 + ((date >> 9) & 0x7f)
    month = (date >> 5) & 0x0f
    day = date & 0x1f
    hour = (time >> 11) & 0x1f
    minute = (time >> 5) & 0x3f
    second = time & 0x1f

    try:
      dt = datetime.datetime(year=year, month=month, day=day,
                             hour=hour, minute=minute, second=second)
    except ValueError:
      dt = None
    return dt

  @staticmethod
  def encodeTimestamp(timestamp):
    year = timestamp.year - 1980
    # Victor MS-DOS is fussy about the year
    #year = 1998 - 1980

    month = timestamp.month
    day = timestamp.day
    hour = timestamp.hour
    minute = timestamp.minute
    second = timestamp.second
    date = ((year << 9) | (month << 5) | day) & 0xFFFF
    time = ((hour << 11) | (minute << 5) | second) & 0xFFFF
    return date, time

  @property
  def isDeleted(self):
    return self.filename[0] == 0xE5

  @property
  def isUnused(self):
    return self.filename[0] == 0x00

  @property
  def isReadOnly(self):
    return bool(self.attributes & DirectoryEntry.ATTRIB_READONLY)

  @property
  def isHidden(self):
    return bool(self.attributes & DirectoryEntry.ATTRIB_HIDDEN)

  @property
  def isSystem(self):
    return bool(self.attributes & DirectoryEntry.ATTRIB_SYSTEM)

  @property
  def isVolumeLabel(self):
    return bool(self.attributes & DirectoryEntry.ATTRIB_VOLUMELABEL)

  @property
  def isSubdir(self):
    return bool(self.attributes & DirectoryEntry.ATTRIB_SUBDIR)

  @property
  def isArchive(self):
    return bool(self.attributes & DirectoryEntry.ATTRIB_ARCHIVE)

  @property
  def path(self):
    p = ""
    b = self.filename
    e = self.extension
    if self.isDeleted:
      p = "*"
      b = self.filename[1:]
    if any(x > 0x7F or x < 0x20 for x in b):
      return None
    base = p + b.decode("utf-8").rstrip()
    ext = e.decode("utf-8").rstrip()
    if len(ext):
      if not self.isVolumeLabel:
        base += "."
      base += ext
    return base

  @path.setter
  def path(self, value):
    ucpath = value.upper()
    ucext = ""
    if '.' in ucpath:
      idx = ucpath.index('.')
      ucext = ucpath[idx + 1:]
      ucpath = ucpath[:idx]

    ucpath = ucpath + "        "
    ucpath = ucpath[:8]
    ucext = ucext + "   "
    ucext = ucext[:3]

    self.filename = ucpath.encode("utf-8")
    self.extension = ucext.encode("utf-8")
    return

  @property
  def created(self):
    return self.decodeTimestamp(self.creationDate, self.creationTime)

  @created.setter
  def created(self, value):
    d, t = self.encodeTimestamp(value)
    self.creationDate = d
    self.creationTime = t

  @property
  def accessed(self):
    return self.decodeTimestamp(self.lastAccessDate)

  @accessed.setter
  def accessed(self, value):
    d, t = self.encodeTimestamp(value)
    self.lastAccessDate = d

  @property
  def modified(self):
    return self.decodeTimestamp(self.lastWriteDate, self.lastWriteTime)

  @modified.setter
  def modified(self, value):
    d, t = self.encodeTimestamp(value)
    self.lastWriteDate = d
    self.lastWriteTime = t

class FAT:
  def __init__(self, image):
    self.image = image

    self.fatLength = 1
    if self.image.label['flags'] & 1 or self.image.sides == 2:
      self.fatLength = 2
    self.clusterSize = 4
    max_fat = (self.image.sector_size * 8 * self.fatLength) // 12
    data = bytearray()
    for s in range(self.fatLength):
      data += self.image.readAbsoluteSector(SECTOR_FAT + s)
    self.fat = self.split12(data)
    self.fat.extend([0] * max_fat)
    self.fat = self.fat[:max_fat]
    self.rootSector = SECTOR_FAT + self.fatLength * 2

    # FIXME - do sanity check and make sure both FATs are identical

    # FIXME - do sanity check and make sure MSDOS.SYS starts at the
    # same sector that boot block specifies.

    self.fat[0] = 0xff8
    self.fat[1] = 0xfff

    return

  def loadFile(self, entry):
    size = entry.fileSize
    data = bytearray()
    chain = self.clusterChain(entry.firstLogicalCluster)
    cluster = chain[0]
    for cluster in chain:
      data += self.readCluster(cluster)
    data = data[:entry.fileSize]
    return data

  def saveFile(self, entry, data):
    if entry.firstLogicalCluster is not None:
      chain = self.clusterChain(entry.firstLogicalCluster)
    else:
      chain = []
    csize = self.image.sector_size * self.clusterSize
    count = (len(data) + csize - 1) // csize
    if count > len(chain):
      chain.extend(self.allocateClusters(count - len(chain)))

    used = []
    offset = 0
    for cluster in chain:
      self.writeCluster(cluster, data[offset:offset + csize])
      offset += csize
      used.append(cluster)

    unused = set(chain) - set(used)

    for c1, c2 in zip(chain, chain[1:]):
      self.fat[c1] = c2
    self.fat[chain[-1]] = 0xFF8
    for cluster in unused:
      self.fat[cluster] = 0

    entry.firstLogicalCluster = chain[0]
    entry.fileSize = len(data)
    entry.modified = datetime.datetime.now()

    self.updateFATSectors()
    return

  def deleteFile(self, entry):
    chain = self.clusterChain(entry.firstLogicalCluster)
    entry.markAsDeleted()
    for cluster in chain:
      self.fat[cluster] = 0
    self.updateFATSectors()
    return

  def updateFATSectors(self):
    raw = self.join12(self.fat)
    ssize = self.image.sector_size
    count = (len(raw) + ssize - 1) // ssize
    if len(raw) < count * ssize:
      raw += bytes([0] * (count * ssize - len(raw)))
    for idx in range(count):
      offset = idx * ssize
      self.image.writeAbsoluteSector(idx + SECTOR_FAT, raw[offset:offset+ssize])
      self.image.writeAbsoluteSector(idx + SECTOR_FAT + self.fatLength,
                                     raw[offset:offset+ssize])
    return

  def allocateClusters(self, count):
    allocated = []
    pos = 2
    for c in range(count):
      while self.fat[pos] != 0:
        pos += 1
      allocated.append(pos)
      pos += 1
    return allocated

  def readCluster(self, cluster):
    data = bytearray()
    sector = self.firstSectorOfCluster(cluster)
    for s in range(self.clusterSize):
      data += self.image.readAbsoluteSector(sector + s)
    return data

  def writeCluster(self, cluster, data):
    sector = self.firstSectorOfCluster(cluster)
    ssize = self.image.sector_size
    count = (len(data) + ssize - 1) // ssize
    for idx in range(count):
      offset = idx * ssize
      sdata = data[offset:offset + ssize]
      if len(sdata) < ssize:
        sdata = sdata + bytes([0] * (ssize - len(sdata)))
      self.image.writeAbsoluteSector(sector + idx, sdata)
    return

  def loadDirectory(self, cluster=0):
    root = []
    chain = self.clusterChain(cluster)
    for cluster in chain:
      data = self.readCluster(cluster)
      entries = self.unpackDirectory(data)
      root.extend(entries)
      if len(entries) < (len(data) // ENTRY_LEN):
        break
    return root

  def createDirectoryEntry(self, directory, path):
    ucpath = path.upper()
    # FIXME - make sure path is legal DOS

    for entry in directory:
      if entry.path == ucpath:
        return entry

    # FIXME - check that there is room for another entry
    entry = DirectoryEntry(None)
    entry.path = ucpath
    directory.append(entry)

    return entry

  def updateDirectory(self, directory, sector=None):
    entries_per_sector = self.image.sector_size // ENTRY_LEN
    if sector is None:
      sector = self.rootSector
    for idx in range(0, len(directory), entries_per_sector):
      entries = directory[idx:idx + entries_per_sector]
      data = self.packDirectory(entries)
      self.image.writeAbsoluteSector(sector + idx // entries_per_sector, data)
    return

  def findEntryForCluster(self, directory, cluster):
    for entry in directory:
      # FIXME - check if entry is directory and recurse
      if entry.firstLogicalCluster == cluster:
        return entry
      chain = self.clusterChain(entry.firstLogicalCluster)
      if cluster in chain:
        return entry
    return None

  def clusterChain(self, index):
    if index == 0:
      return [0, 1]
    chain = []
    while index > 0 and index < 0xff0:
      chain.append(index)
      index = self.fat[index]
    return chain

  def unpackDirectory(self, block):
    entries = []
    for idx in range(0, len(block) // ENTRY_LEN):
      data = block[idx * ENTRY_LEN:idx * ENTRY_LEN + ENTRY_LEN]
      entry = DirectoryEntry(data)
      if entry.isUnused:
        break
      entries.append(entry)
    return entries

  def packDirectory(self, entries):
    empty = bytearray([0] + [0x93] * (ENTRY_LEN - 1))
    sector = empty * (self.image.sector_size // ENTRY_LEN)
    for idx, entry in enumerate(entries):
      data = entry.pack()
      sector[idx * ENTRY_LEN:idx * ENTRY_LEN + ENTRY_LEN] = data
    return sector

  def firstSectorOfCluster(self, cluster):
    sector = cluster * self.clusterSize + self.rootSector
    return sector

  def glob(self, files, directory=None):
    entries = []

    if directory is None:
      directory = self.loadDirectory()

    globbed = []
    for f in files:
      if ':' in f:
        f = f[f.index(':')+1:]

      path = re.split(r"[/\\]", f)
      match = self.matchFile(path[0], directory)
      if not match:
        print(f"'{f}': No such file", file=sys.stderr)
        exit(1)

      if len(path) == 1:
        entries.extend(match)
      else:
        for entry in match:
          if not entry.isSubdir:
            print(f"':{f}': Not a directory", file=sys.stderr)
            exit(1)
          subdir = self.loadDirectory(entry.firstLogicalCluster)
          entry.entries = subdir
          submatch = self.glob(path[1:], subdir)
          for s in submatch:
            if not hasattr(s, 'parent'):
              s.parent = []
              s.directory = entry
            s.parent = [path[0]] + s.parent
          entries.extend(submatch)

    return entries

  @staticmethod
  def matchFile(pattern, directory):
    matched = []
    l_pattern = pattern.lower()
    for entry in directory:
      if entry.isVolumeLabel or entry.isDeleted:
        continue

      path = entry.path.lower()
      if fnmatch.fnmatch(path, l_pattern) \
         or ('.' not in path and fnmatch.fnmatch(path + ".", l_pattern)):
        matched.append(entry)
    return matched

  @staticmethod
  def split12(b):
    val = int.from_bytes(b, "little")
    l = []
    while val:
      l.append(val & 0xFFF)
      val >>= 12
    return l

  @staticmethod
  def join12(l):
    val = 0
    for v in reversed(l):
      val <<= 12
      val |= v & 0xFFF
    n = (len(l) * 12 + 7) // 8
    b = val.to_bytes(n, "little")
    return b
